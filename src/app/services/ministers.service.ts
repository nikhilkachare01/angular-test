import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Minister {
  name: string;
  ministry: string;
  photoURL: string;
}
@Injectable({
  providedIn: 'root'
})
export class MinistersService {

  constructor(private http: HttpClient) { }

  getMinisters(): Promise<Minister[]> {
    return new Promise((resolve, reject) => {
      this.http.get('api/', { responseType: 'text' }).subscribe(text => {
        resolve(this.parseMinistersFromResponse(text));
      }, err => {
        reject(err);
      });
    });
  }

  parseMinistersFromResponse(res): Minister[] {
    const el = document.createElement('div');
    el.innerHTML = res;
    const bioCards = el.getElementsByClassName('bioCard');
    const ministers = [];
    for (let i = 0; i < bioCards.length; i++) {
      ministers.push({
        name: bioCards[i].querySelector('h3 a').textContent,
        ministry: bioCards[i].querySelector('p a').textContent,
        photoURL: bioCards[i].querySelector('img').src
      });
    }
    return ministers;
  }
}
