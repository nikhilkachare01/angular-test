import { Component, OnInit } from '@angular/core';
import { MinistersService, Minister } from './services/ministers.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public ministers: Minister[];
  public selectedMinister: Minister;
  public isLoading = true;

  constructor(
    private ministersController: MinistersService
  ) {}

  listMinisters() {
    this.ministersController.getMinisters().then(ministers => {
      this.isLoading = false;
      this.ministers = ministers;
      this.selectedMinister = ministers[0];
    });
  }

  selectMinister(minister: Minister) {
    this.selectedMinister = minister;
  }

  ngOnInit() {
    this.listMinisters();
  }
}
