import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MinistersService } from './services/ministers.service';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatListModule,
    MatCardModule,
    MatProgressSpinnerModule
  ],
  providers: [MinistersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
